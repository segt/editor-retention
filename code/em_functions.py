import warnings
import pymysql
import pandas as pd
import requests
import io
import datetime
from datetime import date, timedelta
import matplotlib.pyplot as plt
import matplotlib.dates as mdates

def decode_df(df):
    data_types = df.dtypes.to_dict()
    for col in df.columns:
        if data_types[col] == object:
            df[col] = df[col].str.decode('utf-8')
    return df

def connect(wiki_name, cnf_path="/home/paws/.my.cnf"):
    return pymysql.connect(
        host=wiki_name + '.analytics.db.svc.wikimedia.cloud',
        read_default_file=cnf_path,
        database=wiki_name + '_p'
    )

def pd_query(query, wiki):
    warnings.filterwarnings('ignore')
    connection = connect(wiki)
    return decode_df(pd.read_sql(query, connection))

def get_wikis_em(df):
    wikis = df.Wiki.unique().tolist()
    return wikis

def get_database_codes_em(domain_list):
    tsv_file_path = 'data/wikis.tsv'  # Path to the TSV file
    # Create a dictionary to store domain names as keys and corresponding database codes as values
    domain_database_mapping = {}

    # Read the TSV file and populate the dictionary
    tsv_reader = pd.read_table(tsv_file_path, delimiter='\t')
    for index, row in tsv_reader.iterrows():
        domain_database_mapping[row['domain_name']] = row['database_code']

    # Retrieve the database codes for the given domain names
    database_codes = []
    for domain in domain_list:
        domain_with_org = domain + '.org'
        if domain_with_org in domain_database_mapping:
            database_codes.append(domain_database_mapping[domain_with_org])
        else:
            database_codes.append(None)  # Handle the case where domain name is not found in the TSV file

    return database_codes

def query_wikis(query, df, selected_wiki = None):
    if selected_wiki == None:
        wikis = get_wikis(df)
    else:
        wikis = [selected_wiki]
    combined_result = pd.DataFrame()
    for wiki in wikis:
        result = pd_query(query, wiki)
        result['wiki_db'] = wiki
        combined_result = pd.concat([combined_result, result])
        print(f"I'm done with {wiki}")
    return combined_result

def sql_tuple(i):
    if type(i) != list:
        i = [x for x in i]
    if len(i) == 0:
        raise ValueError("Cannot produce an SQL tuple without any items.")
    list_repr = repr(i)
    return "(" + list_repr[1:-1] + ")"

def convert_timestamp(timestamp):
    # Convert the Timestamp object to a datetime object
    dt = timestamp.to_pydatetime()

    # Convert to the desired format
    desired_datetime_str = dt.strftime('%Y%m%d%H%M%S')
    return desired_datetime_str

def generate_query_em(df, days_before = 0, days_after = 0):
    min_date_timestamp = pd.to_datetime(df['Date']).min()
    min_date = convert_timestamp(min_date_timestamp)
    max_date_timestamp =pd.to_datetime(df['Date']).max()
    max_date = convert_timestamp(max_date_timestamp)
    subtracted_date_timestamp = min_date_timestamp - timedelta(days=days_before)
    subtracted_date = convert_timestamp(subtracted_date_timestamp)
    added_date_timestamp = max_date_timestamp + timedelta(days=days_after)
    added_date = convert_timestamp(added_date_timestamp)
    users = df.Username.unique().tolist()
    sql_users = sql_tuple(users)
    query = f"""
    WITH reg_users AS 
        (SELECT
            user_registration,
            user_editcount,
            user_name    
        FROM
            user
        WHERE
            user_name IN {sql_users} AND
            user_registration BETWEEN {subtracted_date} AND {min_date}),
        revision AS (
            SELECT
            COUNT(rev_id),
            actor_name
            FROM
            revision
            JOIN actor ON rev_actor = actor_id
            JOIN reg_users ON reg_users.user_name = actor.actor_name
            WHERE
            rev_timestamp > {max_date} AND
            rev_timestamp < {added_date}
            GROUP BY actor_name
        )
        
        SELECT *
        FROM revision
    """
    print(f"I'm looking for edits between {min_date} & {added_date}")
    print(f"I have consider any users who edited in the campaign and created a Wiki-account between {subtracted_date} & {min_date}")
    return query

def generate_timeline_query_em(df, days_before = 0, days_after = 0):
    min_date_timestamp = pd.to_datetime(df['Date']).min()
    max_date_timestamp = pd.to_datetime(df['Date']).max()
    added_date_timestamp = max_date_timestamp + timedelta(days=days_after)
    added_date = convert_timestamp(added_date_timestamp)
    subtracted_date_timestamp = min_date_timestamp - timedelta(days=days_before)
    subtracted_date = convert_timestamp(subtracted_date_timestamp)
    min_date = convert_timestamp(min_date_timestamp)
    max_date = convert_timestamp(max_date_timestamp)
    users = df.Username.unique().tolist()
    sql_users = sql_tuple(users)
    query = f"""
    WITH reg_users AS 
        (SELECT
            user_registration,
            user_editcount,
            user_name    
        FROM
            user
        WHERE
            user_name IN {sql_users} AND
            DATE(user_registration) BETWEEN {subtracted_date} AND {min_date}),
        revision AS (
            SELECT
            rev_id,
            rev_timestamp,
            actor_name
            FROM
            revision
            JOIN actor ON rev_actor = actor_id
            JOIN reg_users ON reg_users.user_name = actor.actor_name
            WHERE
            rev_timestamp > {max_date} AND
            rev_timestamp < {added_date}
        )
        
        SELECT *
        FROM revision
    """
    print(f"The registration date I used was between {subtracted_date} & {min_date_timestamp}")
    return query

def plot_edits_over_time(df, edit_counts_threshold = 0, fontsize = 0):
    df['rev_timestamp'] = pd.to_datetime(df['rev_timestamp'])
    # Count the number of edits per day
    daily_edits = df.groupby(df['rev_timestamp'].dt.date).size()
    # Create a line plot
    fig, ax = plt.subplots(figsize=(15, 5))
    ax.plot(daily_edits.index, daily_edits.values, marker='o', linestyle='-')
    
    # Format the x-axis as dates
    ax.xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d'))
    ax.xaxis.set_major_locator(mdates.DayLocator())
    
    # Filter the dates to display based on condition
    dates_to_display = daily_edits.index[daily_edits > edit_counts_threshold]
    
    # Set the x-axis ticks to display only for the filtered dates
    ax.set_xticks(dates_to_display)
    
    # Rotate x-axis labels for better readability
    plt.xticks(rotation=45, ha='right')
    plt.tick_params(axis='x', labelsize=fontsize)
    
    # Set labels and title
    plt.xlabel('Date')
    plt.ylabel('Number of Edits')
    plt.title('Edits Over Time')
    
    # Display the plot
    plt.tight_layout()
    plt.show()


def plot_indivedits_over_time_em(df, edit_counts_threshold=0, daily_edits_threshold=0):
    edit_counts = df['actor_name'].value_counts().reset_index()
    edit_counts.columns = ['actor_name', 'total_edits']
    filtered_result = df[df['actor_name'].isin(edit_counts[edit_counts['total_edits'] > edit_counts_threshold]['actor_name'])]
    filtered_result['rev_timestamp'] = pd.to_datetime(filtered_result['rev_timestamp'], format='%Y%m%d%H%M%S')
    grouped_data = filtered_result.groupby(['actor_name', filtered_result['rev_timestamp'].dt.date]).size().reset_index(name='count')
    grouped_data['rev_timestamp'] = pd.to_datetime(grouped_data['rev_timestamp'])  # Ensure 'Date' column is in datetime format

    # Create a line plot for each 'actor_name'
    fig, ax = plt.subplots(figsize=(18, 14))
    for actor_name, data in grouped_data.groupby('actor_name'):
        ax.plot(data['rev_timestamp'], data['count'], marker='o', linestyle='-', label=actor_name, markersize=2)

    # Format the x-axis as dates
    ax.xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d'))
    ax.xaxis.set_major_locator(mdates.DayLocator())

    # Count the number of edits per day
    daily_edits = df.groupby(df['rev_timestamp'].dt.date).size()
    # Filter the dates to display based on condition
    dates_to_display = daily_edits.index[daily_edits > daily_edits_threshold]

    # Set the x-axis ticks to display only for the filtered dates
    ax.set_xticks(dates_to_display)
    plt.xticks(rotation=90, ha='right')
    plt.tick_params(axis='x', labelsize=8)

    # Set labels and title
    plt.xlabel('Date')
    plt.ylabel('Number of Edits')
    plt.title('Edits Over Time')

    # Add a legend
    plt.legend()

    # Display the plot
    plt.tight_layout()
    plt.savefig('plot.png')
    plt.show()
