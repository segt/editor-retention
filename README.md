# Editor retention, an actionable code approximation


## Usage & description

This repo contains a ready to use python notebook to understand which editors kept editing after being trained in an event captured in the Outreach Dashboard.
You can download the code and import it your PAWS account (login with your existing MediaWiki account).

## Support
This project is not currently being develop further, but if we find out there is a strong interest, we might allocate more resources. So please, let us know about what you would like to see in the future :) 

## Contributing
If you're willing to contribute to this project to make it better do not hesitate to [contact me](https://meta.wikimedia.org/wiki/User:SEgt-WMF).

## Authors and acknowledgment
This code was created by [Silvia Gutiérrez](https://meta.wikimedia.org/wiki/User:SEgt-WMF) with contributions & support from [Krishna Chaitanya (KC) Velaga](https://www.linkedin.com/in/kcvelaga/?originalSubdomain=in).

## License
[CC-BY-4.0](https://creativecommons.org/licenses/by/4.0/deed.en)

